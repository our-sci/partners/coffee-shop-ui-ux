coffeeShop = {};

const communityList = [ // list of all communities in the coffee shop
    {
        name: "bobs farm club",
        description: "",
        requiredWorkflow: [], // if set as primary community for user, add this target to their targetList    
    }
]

const userProfile = {
    workflow: [ // when use fills out yearly planting registration survey, objects are added to this list
        { 
            name: 'soil infiltration',
            type: 'measurement',
            urls: [
                "URL link to form"
            ],
            dates: ["list of date(s) when it should be filled out"], // selected by user
        },
        { // option 1: yearly.  Single survey.
            name: 'Yearly',
            type: 'measurement',
            urls: [
                "URL link to yearly form"
            ],
            dates: ["list of date(s) when it should be filled out"], // selected by user
        }
    ],
    communities: [
        {
            name: 'johns farm club',
            isPrimary: 1, // primaray community defines the measurement 
        },
        {
            name: 'colorado wheat growrs',
            isPrimary: 0,
        }
    ],
    contextList: [], // series of filters used in data vis.
};

const workflow; {
    
}

const workflowList = { // list of acceptable management data uploading options
    yearly: [
        { // option 1: yearly.  Single survey.
            name: 'Yearly',
            type: 'management',
            urls: [
                "URL link to yearly form"
            ],
            datesOptions: ["allowable dates to fill this out"], // user will choose from within this set of parameters
        }
    ],
    plantingHarvest: [ // option 2: planting and harvest.  Order is set by order of objects in array (planting is first, then harvest)
        {
            name: "Planting",
            type: 'management',
            urls: [
                "URL link to planting form"
            ],
            datesOptions: ['allowable dates to fill this out'], // user will choose from within this set of parameters
        },
        {
            name: "Harvest",
            type: 'management',
            urls: [
                "URL link to harvest form"
            ],
            datesOptions: ['allowable dates to fill this out'], // user will choose from within this set of parameters
        }
    ],
    soilInfiltration: [
        { 
            name: 'soil infiltration',
            type: 'measurement',
            urls: [
                "URL link to form"
            ],
            datesOptions: ['allowable dates to fill this out'], // user will choose from within this set of parameters
        }    
    ]
};

const goalList = [

];

const measurementList = [

];

// workflow is a data structure which organizes
// by date and order... 
